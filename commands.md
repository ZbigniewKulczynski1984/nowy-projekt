# Przydatne polecenia GIT
​
## basics
​
* **git init** - inicjalizuje repozytorium GIT w katalogu
* **git clone {adres repozytorium}** - klonuje repozytorium do katalogu
* **git status** - pokazuje status repozytorium (pokazuje informację o zmodyfikowanych, nowych, usuniętych oraz nie należące do repozytorium plikach)
* **git add {ścierzka do pliku}** - dodaje plik do repozytorium (np. `git add folder/plik.php`)
* **git add -A** - dodaje wszystkie nie należące do repozytorium pliki
* **git config --global color.ui auto** - włącza koloryzowanie wyników w konsoli
* **git config --global core.pager '{nazwa}'** - ustawia program do przeglądania logów (brak w konsoli)
​
## repop
​
* **git fetch -p** - kasuje branche już nie istniejąca na głównym repo
* **git fetch {nazwa remota}** - pobiera listę zmian z innego repozytorium (w tym pokazuje nowe gałęzie)
* **git remote -v** - lista repo
* **git remote remove {repo}** - usuwa wskazane repo
* **git remote set-url {nazwa repo} {url}** - zmienia adres dla podanego repo
* **git remote add {jakaś nazwa} {adres repozytorium}** - dodaje repozytorium innego użytkownika (`git remote add upstream https://github.com/bluetree-service/idylla.git`)
* **git remote -v** lista wszystkich zewnetrznych repozytoriów
* **git remote rm {nazwa dla remota}** - usuwa zewnętrzne repozytorium
* **git pull** - pobiera zmiany z aktualnej gałęzi
* **git pull {nazwa gałęzi}** - pobiera zmiany z wybranej gałęzi
* **git pull {nazwa remota} {nazwa gałęzi}** - pobiera zmiany z wybranej gałęzi wybranego zewnętrznego repozytorium
* **git pull --all --prune** - ściąga zmiany z repo + kasuje nieużywane branche
* **git pull --tags** - ściąga tagi
* **git pull --rebase** - 
* **git push** - wypycha zmiany na aktualnie wybraną gałąź
* **git push {nazwa gałęzi}** - wypycha zmiany na wskazaną gałąź
* **git push {nazwa remota} {nazwa gałęzi}** - wypycha zmiany na gałąź wskazanego repozytorium
* **git push --tags** - wysyła tagi na repo
* **git revert -- {plik}** - revert pojedyńczego pliku
* **git reset --soft HEAD^** - cofa zmiany bez usuwania dodanych plików
* **git reset --soft {numer commita}** - cofa zmiany bez usuwania dodanych plików do wskazanego commita (`git reset --soft b87dcea`)
* **git reset --hard {numer commita}** - cofa zmiany włącznie z usunięciem plików do wskazanego commita (`git reset --hard b87dced`)
* **git reset --merge ORIG_HEAD** - resetuje zmiany z ostatniego merg-a
* **git checkout -- {plik}** - przywraca oryginalny plik
* **git checkout {commit} -- {plik}** - przywraca stan pliku ze wskazanego commita
* **git checkout -b {nazwa brancha} origin {nazwa brancha}** - tworzy brancha lokalnie i pobiera go (z aktualnymi zmianami) ze zdalnego miejsca
* **git ls-files** - lista plików z ich ścierzkami w repo (-md + zmodyfikowane i usunięte)
* **git rm --cached** - usuwa z pliki z listy do commitów
* **git rm {plik}** - kasuje z repo plik
* **git rm {plik}** - usuwa plik z repo
​
## commit
​
* **git commit** - tworzy commita z aktualnie zmienionych plików
* **git commit -m "wiadomosc"** - tworzy commmita z podaną w cudzysłowach wiadomością
* **git commit --amend -m "{wiadomość}"** - umożliwia zmianę ostatniego commita
* **git commit --amend -m "dsfsdf"** - modyfikuje komentarz ostatniego commita
* **git commit --date="2017-08-18T13:23:41" -m ""** - comit ze wskazaną datą
* **git commit -n** - pomija git hooks
* **git revert {numer commita}** - tworzy nowego commita z cofnięciem zmian ze wskazanego commita
* **git amend** - zmienia poprzedniego commita
​
## log
​
* **git log** - wyświetla listę commitów (od najnowszego)
* **git log -{numer}** wyświetla podha
* **git branch -a** - pokazuje listę wszystkich gałęzi (łącznie z tymi z repo, same z repo `-r`)
​
## stash
​
* **git stash** - zapisuje nowe i zmodyfikowane pliki do pamięci podręcznej
* **git stash pop** - przywraca zapisane pliki z pamięci podręcznej
* **git stash pop --1**
* **git stash pop --index 1**
* **git stash pop --index 454aa619**
* **git stash pop --index stash@{1}**
* **git stash pop --index {1}**
* **git stash pop --index=1**
* **git stash pop -0**
* **git stash pop -1**
* **git stash pop -2**
* **git stash pop 1**
* **git stash pop 454aa619**
* **git stash pop stash@{1}**
* **git stash save "ma-sdapibau-537"**
* **git stash show stash@{1}**
* **git stash show stash@{2}**
* **git stash list** - lista zachowanych zmian
* **git stash drop stash@{x} - usuwa stash, x - numer stasha
​
## tag
​
* **git tag -l** - lista tagów
* **git tag -a {} -m '{ commity w* - pokazuje commity danego użytkownika
* **git shuthor={nazwa}** re=2016-ezt mmitach
* **git log --no-merges --pretty=format:'%C(--ającego podkazuze wskazłączenie (możliwe, gdy wystąpią konflikt zapisuje zmiany
* **git merge --revert** - cofa wszystkie wprowadz której obecnie jesteś z zachowaniem kolejności wprowadzania zmian
* **git rebase {nazwa remotaozytorium i gałęzi z zachowanięzi1} {nazwa gałęzi2}** - dołącz -C 10 {fraza}** - jw aleommitami
* **giny wh** - lista gałęzi w repozytną gałąź
* **git ranche
* **git branch -a** - t $1}' | egrep -v -f /dev/fd/0 <s git branch -d** - kasuje wszystkie nie używane/ nie istniejące branche
* **giałęzi}** - tworzy nową gałąź o podanej nazw podanym pli na commita wyżej, wybierz 2 rod*git tag --sort=v:refname |  tail -2 | xari tagami (| *git tag -l "{pattern}"**& git push origin :refs/tags/{tag}*pick {commi1} {commit2}** - kopiuje commity  kow
