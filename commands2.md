# Przydatne polecenia GIT
​
## basics
​
* **git init** - inicjalizuje repozytorium GIT w katalogu
* **git clone {adres repozytorium}** - klonuje repozytorium do katalogu
* **git status** - pokazuje status repozytorium (pokazuje informację o zmodyfikowanych, nowych, usuniętych oraz nie należące do repozytorium plikach)
* **git add {ścierzka do pliku}** - dodaje plik do repozytorium (np. `git add folder/plik.php`)
* **git add -A** - dodaje wszystkie nie należące do repozytorium pliki
* **git config --global color.ui auto** - włącza koloryzowanie wyników w konsoli
* **git config --global core.pager '{nazwa}'** - ustawia program do przeglądania logów (brak w konsoli)
​
## repop
​
* **git fetch -p** - kasuje branche już nie istniejąca na głównym repo
* **git fetch {nazwa remota}** - pobiera listę zmian z innego repozytorium (w tym pokazuje nowe gałęzie)
* **git remote -v** - lista repo
* **git remote removeckety)
* **git tag -l "{pattern}"** - lista tagów pasująca do wzorca
* **git tag -d {tag} && git push origin :refs/tags/{tag}** - kasuje taga lokalnie + repo
* **git lasttag** - pokazuje ostatniego taga
​
## cherry-pick
​
* **git cherry-pick {commi1} {commit2}** - kopiuje commity w kolejności w jakiej je zapisałeś do brancha na którym jesteś. {repo}** - usuwa wskazane repo
* **git remote set-url {nazwa repo} {url}** - zmienia adres dla podanego repo
* **git remote add {jakaś nazwa} {adres repozytorium}** - dodaje repozytorium innego użytkownika (`git remote add upstream https://github.com/bluetree-service/idylla.git`)
* **git remote -v** lista wszystkich zewnetrznych repozytoriów
* **git remote rm {nazwa dla remota}** - usuwa zewnętrzne repozytorium
* **git pull** - pobiera zmiany z aktualnej gałęzi
* **git pull {nazwa gałęzi}** - pobiera zmiany z wybranej gałęzi
* **git pull {nazwa remota} {nazwgit show {commit}** - szczegóły podanego commita
* **git show --name-only {commit}** - nazwy smodyfikowanych plików w commicie
* **git show --name-only {commit}** - pokazuje tylko liste zmodyfikowanych plików z commita
* **git show {commit}** - pokazuje zmieny w commicie
* **git show HEAD:{plik}** - pokazuje zmiany tylko w konkretnym pliku
​
## branch
​
* **git branch** - lista gałęzi w repozytorium
* **git branch -d {nazwa gałęzi}** - usuwa wskazaną gałąź
* **git branch --merged** - lista zmergowanych branchy
* **git branch --merged | git branch -d** - kasuje wszystkie zmergowane branche
* **git branch -a** - lista branchy + remote info
* **git branch -r | awk '{print $1}' | egrep -v -f /dev/fd/0 <(git branch -vv | grep origin) | awk '{print $1}' | xargs git branch -d** - kasuje wszystkie nie używane/ nie istniejące branche
* **git brnach -m {new_name}** zmiana nazwy brancha na który jesteś
* * - pokazuje commity wykonane przez autora wraz ze zmodyfikowanymi plikami
* **git log master..develop** - pokazuje różnicę między branchami
​
## merge
​
* **git merge {nazwa gałęerge {nazwa remota}/{nazwa gałęzi}** - dołączenie zmian ze wskazanego remota i gałęzi
* **git merge --abort** - przerywa łączenie (możliwe, gdy wystąpią konflikty)
* **git merge --continue** - po rozwiązaniu konflitów zapisuje zmiany
* **git merge --revert** - cofa wszystkie wprowadzone zmiany
​
## rebase
​
* **git rebase {nazwa gałęzi}** - dołączenie zmian do wskazanej gałęzi z gałęzi na której obecnie jesteś z zachowaniem kolejności wprowadzania zmian
* **git rebase {nazwa remota}/{nazwa gałęzi}** - dołączenie zmian ze wskazanego repozytorium i gałęzi z zachowaniem kolejności wprowadzania zmian
* **git rebase {nazwa gałęzi1} {nazwa gałęzi2}** - dołącza głąź2 do gałęzi1 z zachowaniem kolejności wprowadzonych zmian comity danego autora
* **git llog --after=2016-08-16 --before=2016-08-30** - commity z podanego zakresu (--until starsze od podanej daty, --since z pred podanej daty)
* **git log --name-status** - podaje statuz zmian przy nazwie pliku (add, mod, delete)
* **git log --stat** - + statystyki zmian w plikach (--shortstat bez ++++---)
* **git log {commit1}..{commit} --no-merges** - pokazuje zmiany pomiędzy 2 commitami bez info o mergach
* **git log -- {plik/katalog}** - log dla pojedyńczego pliku lub wszystkich plików z katalogu
* **git log -5 --pretty=tformat: --numstat** - satystyki zmian w 5 commitach
* **git lCblue%an%Cgreen%d %Creset%s' --date=iso
* **git log --pretty=format:'* %s (%an)' -n 10** - pokazuje tylko nazwy commitów
* **git log --pretty=oneline -15 | awk '{print $2}' | sort | uniq | grep -i {ticket} | sed 's/\[\(.*\)\]/\1/g'** - pokaże tylko nazwy ticketów (gdy message zgody z formatem [NAME-111] som"SOC-[
* **git pull --rebase** - 
* **git push** - w*git push {nazwa rzanego romazmerge ORIG_HEAD** - resetuje zmiany z ostatniegln-md + zmodpo pkuje komentarz ostatniego commita
* **git commit -- datą
* **git commit -n** - pomija git hooks
* **git revert {n commitów (od najnowszego)
* **git log -{numer}** wyświetla podaną liczbę ostatnich commi skróconej
* **git log -{nuw w postaci skróconej
* **git log --ge --interactive {commit}*** **git diff --ę m*git diff-index --name-only --c** - rożnica od podanmaster" | xargs git branch -D*anch {nazwa gałęzi1} {nazwa gałzi2 o ^^2^ (^- jeden commit do wyżgit branch -f {nazwa gałęzi} {nazwa commi 
* **git branch rename {stara nazwa} {nowa natash pop**ash pop --1**
* **git stash pindex {1}**
* **git stash pop --index=1tash pop 454aaave "ma-sdapibau-537"**
* *{2}**
* **git stash list** - lit tag --sort=v:refname |  tail -2 | xargs printf \"

bla bla bla
